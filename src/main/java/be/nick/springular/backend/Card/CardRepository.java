package be.nick.springular.backend.Card;

import be.nick.springular.backend.Security.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findOneByOwner(User user);

}
