package be.nick.springular.backend.Rest;

import be.nick.springular.backend.Card.Card;
import be.nick.springular.backend.Card.CardRepository;
import be.nick.springular.backend.Security.AuthoritiesConstants;
import be.nick.springular.backend.Security.User.User;
import be.nick.springular.backend.Security.User.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class CardController {

    private final CardRepository cardRepository;
    private final UserRepository userRepository;

    public CardController(CardRepository cardRepository, UserRepository userRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }
    @GetMapping("/all")
    @Secured(AuthoritiesConstants.USER)
    List<Card> getAllCards(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findOneByUsername(authentication.getName()).get();
        return cardRepository.findOneByOwner(user);
    }
    @PostMapping("/add")
    @Secured(AuthoritiesConstants.USER)
    @ResponseStatus(HttpStatus.CREATED)
    void addCard(@RequestBody Card card){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            user = userRepository.findOneByUsername(authentication.getName()).get();
        }
        card.setOwner(user);
        cardRepository.save(card);
    }
    @PutMapping("/edit")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void updateCard(@RequestBody Card card){
        cardRepository.save(card);
    }
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteCard(@PathVariable long id){
        cardRepository.delete(id);
    }


}
