package be.nick.springular.backend.Security.User;

import be.nick.springular.backend.Security.AuthoritiesConstants;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User registerUser(User appUser) {

        User newUser = new User();
        Set<String> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(appUser.getPassword());
        newUser.setUsername(generateUsername());
        newUser.setPassword(encryptedPassword);
        newUser.setEmail((appUser.getEmail()));
        newUser.setFirst_name(appUser.getFirst_name());
        newUser.setLast_name(appUser.getLast_name());
        newUser.setProfile_pic_url(appUser.getProfile_pic_url());
        newUser.setActivated(true);
        newUser.setActivationKey(RandomStringUtils.randomNumeric(20));
        authorities.add(AuthoritiesConstants.USER);
        if(userRepository.findAll().size()==0){ //todo NOT SO SAFE...
            authorities.add(AuthoritiesConstants.ADMIN);
        }
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        return newUser;
    }

    private String generateUsername(){
        String randomUserName = "user-"+UUID.randomUUID().toString().substring(0,13);
        if(userRepository.findOneByUsername(randomUserName).isPresent()){
            randomUserName=generateUsername();
        }
        return randomUserName;
    }

}
